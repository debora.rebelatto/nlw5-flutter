import 'package:flutter/material.dart';

import 'package:dev_quiz/templates/core/app_colors.dart';
import 'package:dev_quiz/templates/core/app_images.dart';
import 'package:dev_quiz/templates/core/app_text_styles.dart';
import 'package:dev_quiz/templates/shared/widget/progress/progess_indicator.dart';

class QuizCardWidget extends StatefulWidget {
  @override
  _QuizCardWidgetState createState() => _QuizCardWidgetState();
}

class _QuizCardWidgetState extends State<QuizCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.fromBorderSide(
            BorderSide(color: AppColors.border),
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 40,
              width: 40,
              child: Image.asset(AppImages.blocks)
            ),
            SizedBox(height: 24),
            Container(
              child: Text('Gerenciamento de Estado',
                style: AppTextStyles.heading15,
              ),
            ),
            SizedBox(height: 20),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text('3 de 10',
                    style: AppTextStyles.body11,
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: ProgressIndicatorWidget(.3)
                )
              ]
            )
          ],
        )
      ),
    );
  }
}
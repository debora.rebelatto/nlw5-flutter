import 'package:flutter/material.dart';
import 'package:dev_quiz/templates/challenge/widgets/answer/asnwer_widget.dart';
import 'package:dev_quiz/templates/core/app_text_styles.dart';

class QuizWidget extends StatefulWidget {
  final String title;

  QuizWidget(this.title);

  @override
  _QuizWidgetState createState() => _QuizWidgetState();
}

class _QuizWidgetState extends State<QuizWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 5),
              child: Text(
                widget.title,
                style: AppTextStyles.heading,
              ),
            ),
            SizedBox(height: 20,),

            AnswerWidget(
              title: 'this is the title'
            ),
            AnswerWidget(
              isRight: false,
              isSelected: true,
              title: 'this is the title'
            ),
            AnswerWidget(
              title: 'this is the title'
            )

          ],
        )

      ),
    );
  }
}
import 'package:dev_quiz/templates/core/app_text_styles.dart';
import 'package:dev_quiz/templates/shared/widget/progress/progess_indicator.dart';
import 'package:flutter/material.dart';

class QuestionIndicator extends StatefulWidget {
  @override
  _QuestionIndicatorState createState() => _QuestionIndicatorState();
}

class _QuestionIndicatorState extends State<QuestionIndicator> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Questão 06',
                style: AppTextStyles.body,
              ),
              Text('de 10',
                style: AppTextStyles.body,
              )
            ],
          ),
          SizedBox(height: 16),
          ProgressIndicatorWidget(.3)
        ],
      )
    );
  }
}
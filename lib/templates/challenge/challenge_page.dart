import 'package:dev_quiz/templates/challenge/widgets/question_indicator.dart';
import 'package:dev_quiz/templates/challenge/widgets/quiz_widget/quiz_widget.dart';
import 'package:flutter/material.dart';

class ChallengePage extends StatefulWidget {
  @override
  _ChallengePageState createState() => _ChallengePageState();
}

class _ChallengePageState extends State<ChallengePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: QuestionIndicator(),
      ),
      body: Container(
        child: QuizWidget('title')
      ),
    );
  }
}